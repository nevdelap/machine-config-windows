### To use this:

* Create a 'Portable' directory on the Desktop.
* Download the 64 bit portable versions of Git Bash and Sublime Text 3.
     - https://git-scm.com/download/win
     - https://www.sublimetext.com/3
* Extract them as 'Git' and 'Sublime Text' in 'Portable', which are the directories that are source controlled and contain their config.
* Run in Git Bash:
```
        cd "/c/Users/$USERNAME/Desktop/Portable"
        git clone git@bitbucket.org:nevdelap/machine-config-windows.git
        # or, git clone https://username@bitbucket.org/nevdelap/machine-config-windows.git
        mv machine-config-windows/.git .
        rm -rf machine-config-windows
        git reset --hard
        .bin/setup
```
* Exit and rerun Git Bash.
(Optional)
* Run Sublime Text.
* Select Tool > Install Package Control... It will install it and all of the packages in the source controlled configuration.

### New version:

I've just installed MSYS2 and it is probably better. It has fish shell! (Which is so way better than bash for usability.)

* Download and install the 64 bit version of MSYS2.
     - http://www.msys2.org/
* Run in MSYS2:
```
        pacman -S git nano python2
        # In the latest update there is an issue with the 31/1 update.
        # https://github.com/Alexpux/MSYS2-packages/issues/1564
        # Downgrade pcre2 and install fish 2.7.1 instead.
        pacman -Syuu pcre2
        wget http://repo.msys2.org/msys/x86_64/fish-2.7.1-2-x86_64.pkg.tar.xz
        pacman -U fish-2.7.1-2-x86_64.pkg.tar.xz
        git clone git@bitbucket.org:nevdelap/machine-config-windows.git
        # or, git clone https://username@bitbucket.org/nevdelap/machine-config-windows.git
        mv machine-config-windows/.git .
        rm -rf machine-config-windows
        git reset --hard
```
* Run:
```
        fish
```

### Hints

* Drives are mounted at /.

```
        cd /c/mydir # C:\mydir
```

### TODO
* Figure out how to make msys2 use fish as the default shell. For now it starts bash and you type `fish`.
* Investigate a portable version.

### Scripts

```
abbrs # Python script to convert bash git scripts to fish abbreviations. Requires fish 3.0.0.
c     clear
g     clear; gll
ga    git add
gaa   git add .
gb    git branch -a -v
ga    git commit -m
gca   git commit --amend
gcaq  git commit --amend --no-edit (quite)
gcfd  git clean -f -d
gco   git checkout
gcom  git checkout origin/master
gcp   git cherry-pick
gcpa  git cherry-pick --abort
gcpc  git cherry-pick --continue
gd    git diff --color-words=. -w 2> /dev/null
gdc   git diff --cached --color-words=. -w 2> /dev/null
gdrb  git push -d origin # Delete remote branch.
gf    git fetch
gfsck git fsck
gc    git gc --prune=now
gll   git log --pretty=format:"%Cgreen%h %C(yellow)[%cd]%Cred%d %Creset%s%Cblue [%cn]" --decorate --date=local HEAD --not origin/master~1 # Log stopping at master.
glll  git log --pretty=format:"%Cgreen%h %C(yellow)[%cd]%Cred%d %Creset%s%Cblue [%cn]" --decorate --date=local
glg   git log --graph --pretty=format:"%Cgreen%h %C(yellow)[%cd]%Cred%d %Creset%s%Cblue [%an]" --decorate --date=local HEAD # Log with graph.
glls  git log -10 --pretty=format:"%C(green)%h %C(yellow)[%ad]%Cred%d %Creset%s%Cblue [%cn]" --decorate --date=relative --stat
gls   git log -10 --name-status
gp    git push
gpc   # Git push command. Push everything automatically that begins with TOT- or DOCS- to matching branch, creating it if necessary, until it hits a merge.
gpf   git push -f
gpo   git remote prune origin
gpof  git push -f origin
gpom  git push origin HEAD:master
gra   git rebase --abort
grc   git rebase --continue
grh   git reset --hard
gri   git rebase --interactive
griom git rebase -i origin/master
grl   git reflog --pretty=format:"%Cgreen%h %C(yellow)[%cd]%Cred%d %Creset%s%Cblue [%cn]" --decorate --date=local # Reflog in the same format as gll. (msys2 version only.)
grm   git rm
grom  git rebase origin/master
gs    git status
gsp   git stash pop
gst   git stash
ned   Terminal batch search and replace.
```
