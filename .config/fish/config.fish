if test -d ~/totalcare_documentation.local/bin
    set -gx PATH ~/totalcare_documentation.local/bin $PATH
end
set -gx PATH ~/.bin $PATH
set -gx GIT_EDITOR "subl -n -w"
set -gx NED_DEFAULTS "-Ru --colors=always"
